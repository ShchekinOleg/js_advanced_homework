'use strict';

const request = fetch('https://ajax.test-danit.com/api/swapi/films');

const spinner = document.createElement('div');
spinner.className = 'lds-dual-ring';
document.body.append(spinner);

const postsEl = document.createElement('div');
document.body.append(postsEl);

request
    .then(response => {
        return response.json()
    })
    .then(posts => {
        console.log(posts);
        const list = posts.map(({episodeId, name, openingCrawl, characters}) => {
            const div = document.createElement('div');
            const h2 = document.createElement('h2');
            const h5 = document.createElement('h5');
            const p = document.createElement('p');

            h2.innerText = 'Episode: ' + episodeId;
            h5.innerText = 'Title: ' + name;
            p.innerText = 'Description: ' + openingCrawl;

            const listChar = document.createElement('ul');

            Promise.all(characters.map(characterUrl => {
                return fetch(characterUrl)
                    .then(response => response.json())
                    .then(({name}) => {
                        const li = document.createElement('li');
                        li.innerText = name;
                        return li;
                    })
            }))
                .then(li => {
                    listChar.append(...li);
                });


            div.append(h2, h5, p, listChar);
            return div
        });
        spinner.remove();
        postsEl.append(...list)
    });

