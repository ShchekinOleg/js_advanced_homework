"use strict";

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
}

Object.defineProperties(Employee, {
    'name': {
        get: function () {
            return this._name
        },
        set: function (value) {
            this._name = value
        }
    },
    'age': {
        get: function () {
            return this._age
        },
        set: function (value) {
            this._age = value
        }
    },
    'salary': {
        get: function () {
            return this._salary
        },
        set: function (value) {
            this._salary = value
        }
    }
});

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary * 3);
        this.lang = lang;
    }
}

Object.defineProperties(Programmer, {
    'salary': {
        get: function () {
            return this._salary
        },
        set: function (value) {
            this._salary = value
        }
    }
});

const programmer1 = new Programmer('Nick', 30, 3000, ['Java', 'SQL']);
const programmer2 = new Programmer('Tom', 40, 2000, ['HTML', 'CSS', 'JavaScript']);

console.log(programmer1);
console.log(programmer2);